import { initializeApp } from "firebase/app";
import { getAuth, GoogleAuthProvider, EmailAuthProvider, signOut, onAuthStateChanged,
    // Uncomment to use auth emulator (another uncomment below)
    // , connectAuthEmulator
} from "firebase/auth";
import * as firebaseui from 'firebaseui';
import 'firebaseui/dist/firebaseui.css';
const firebaseConfig = {
    apiKey: "AIzaSyDaf_mk7lkOlVbdrgir04D8xt1y-yhoxz8",
    authDomain: "cradle-bio.firebaseapp.com",
    projectId: "cradle-bio",
    storageBucket: "cradle-bio.appspot.com",
    messagingSenderId: "473699240866",
    appId: "1:473699240866:web:dc11fa69bf2599f56b80fb",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth(app);
// Uncomment to use auth emulator
// connectAuthEmulator(auth, "http://localhost:9099");
const ui = new firebaseui.auth.AuthUI(auth);

// Sign in buttons
const uiConfig = {
    callbacks: {
        signInSuccessWithAuthResult: function (authResult, redirectUrl) {
            signedIn();
            return false;
        },
        signInFailure: function (error) {
            // Some unrecoverable error occurred during sign-in.
            // Return a promise when error handling is completed and FirebaseUI
            // will reset, clearing any UI. This commonly occurs for error code
            // 'firebaseui/anonymous-upgrade-merge-conflict' when merge conflict
            // occurs. Check below for more details on this.
            return console.log(error);
        },
    },
    credentialHelper: firebaseui.auth.CredentialHelper.NONE,
    signInOptions: [
        GoogleAuthProvider.PROVIDER_ID,
        {
            provider: EmailAuthProvider.PROVIDER_ID,
            // Use email link authentication and do not require password.
            // Note this setting affects new users only.
            // For pre-existing users, they will still be prompted to provide their
            // passwords on sign-in.
            signInMethod: EmailAuthProvider.EMAIL_LINK_SIGN_IN_METHOD,
            // Allow the user the ability to complete sign-in cross device, including
            // the mobile apps specified in the ActionCodeSettings object below.
            forceSameDevice: false,
            // Used to define the optional firebase.auth.ActionCodeSettings if
            // additional state needs to be passed along request and whether to open
            // the link in a mobile app if it is installed.
            emailLinkSignIn: function() {
                return {
                    url: window.location.href,
                    // Always true for email link sign-in.
                    handleCodeInApp: true
                };
            }
        }
    ],
    immediateFederatedRedirect: false,
    // tosUrl and privacyPolicyUrl accept either url string or a callback
    // function.
    // TODO we should get these
    // Terms of service url/callback.
    // tosUrl: '<your-tos-url>',
    // // Privacy policy url/callback.
    // privacyPolicyUrl: <your-privacy-policy-url>'
};

function downloadCredentials(accessToken) {
    const downloader = document.createElement('a');
    document.body.appendChild(downloader);
    downloader.setAttribute('download', 'cradle_credentials.json');
    const content = JSON.stringify({
        refresh_token: auth.currentUser.refreshToken,
        token: accessToken,
        api_key: firebaseConfig.apiKey
    }, null, '  ');
    downloader.setAttribute(
        'href',
        'data:application/json;charset=utf-8,' + encodeURIComponent(content));
    downloader.click();
}

function signedIn() {
    document.getElementById("download-credentials").style.display = "block";
    document.getElementById("sign-out-button").style.display = "block";
}

function signedOut() {
    document.getElementById("download-credentials").style.display = "none";
    document.getElementById("sign-out-button").style.display = "none";
    ui.start('#firebaseui-auth-container', uiConfig);
}

window.addEventListener('load', function () {

    document.getElementById("download-credentials").addEventListener(
        "click",
        function(){
            // No-op if user isn't set. Though they shouldn't be able to find and click
            // button anyhow.
            if (auth.currentUser) {
                // Force a token refresh to ensure the user gets fresh credentials
                // with each click.
                auth.currentUser.getIdToken(true).then(downloadCredentials);
            }
        },
        false);

    document.getElementById("sign-out-button").addEventListener(
        "click",
        function (){
            signOut(auth).then(() => {
                console.log('Signed Out');
            }).catch(e=>{
                console.error('Sign Out Error', e);
            });
        });

    // loading page with a logged-out user triggers start of ui element.
    onAuthStateChanged(auth, function(user){
       if (user) {
           signedIn();
       } else {
           signedOut();
       }
    });
    ui.disableAutoSignIn();
});